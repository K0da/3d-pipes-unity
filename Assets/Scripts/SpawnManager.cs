using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject tubePrefab;
    public Vector3 startingPosition = Vector3.zero;
    public int timesSpawing = 5;
    private int pipeCounter = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        generateACompletePipe(5);
    }

    private GameObject instantiateNewTubeSegment(Vector3 position, Quaternion rotation)
    {
        return Instantiate(tubePrefab, position, rotation);
    }

    private Vector3 findNextForwardPosition(Vector3 starterPosition)
    {
        return starterPosition + new Vector3(0, tubePrefab.transform.lossyScale.y, 0);
    }

    private void createPipe(int numberOfTiles, GameObject prefab)
    {
        GameObject pipe = new GameObject("Pipe " + pipeCounter);
        pipeCounter++;

        var latestSegmentPosition = prefab.transform.position;
        Instantiate(prefab).transform.parent = pipe.transform;
        
        
        for (int i = 0; i < numberOfTiles; i++)
        {
            var newTubeSegment = Instantiate(
                prefab,
                findNextForwardPosition(latestSegmentPosition),
                prefab.transform.rotation);
            latestSegmentPosition = newTubeSegment.transform.position;
            newTubeSegment.transform.parent = pipe.transform;
        }
    }

    private GameObject rotatePipeSegmentRandomly(GameObject pipeSegment)
    {
        var pipeRotation = pipeSegment.transform.rotation;
        
        switch (Random.Range(0, 3))
        {
            case 0:
                break;
            case 1:
                pipeRotation.y += 90;
                break;
            case 2:
                pipeRotation.y -= 90;
                break;
        }
        
        switch (Random.Range(0, 3))
        {
            case 0:
                break;
            case 1:
                pipeRotation.x += 90;
                break;
            case 2:
                pipeRotation.x -= 90;
                break;
        }
        
        pipeSegment.transform.rotation = pipeRotation;
        return pipeSegment;
    }

    void generateACompletePipe(int angles)
    {
        var segment = rotatePipeSegmentRandomly(tubePrefab);
        
        for (int i = 0; i < angles; i++)
        {
            createPipe(timesSpawing, startingPosition);
        }
    }
}
