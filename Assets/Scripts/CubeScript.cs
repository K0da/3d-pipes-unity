using UnityEngine;

public class CubeScript : MonoBehaviour
{
    // Declare variables for the Mesh and MeshFilter
    Mesh mesh;
    MeshFilter meshFilter;

    void Start()
    {
        // Initialize the Mesh and MeshFilter
        mesh = new Mesh();
        meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        // Create the vertices of the cube
        Vector3[] vertices = new Vector3[8]
        {
            new Vector3(-1, -1, 1),
            new Vector3(1, -1, 1),
            new Vector3(1, 1, 1),
            new Vector3(-1, 1, 1),
            new Vector3(-1, -1, -1),
            new Vector3(1, -1, -1),
            new Vector3(1, 1, -1),
            new Vector3(-1, 1, -1)
        };

        // Create the triangles of the cube
        int[] triangles = new int[36]
        {
            0, 1, 2, 0, 2, 3, // front
            4, 5, 6, 4, 6, 7, // back
            3, 2, 6, 3, 6, 7, // top
            1, 0, 4, 1, 4, 5, // bottom
            1, 5, 6, 1, 6, 2, // right
            0, 3, 7, 0, 7, 4  // left
        };

        // Assign the vertices and triangles to the Mesh
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }
}