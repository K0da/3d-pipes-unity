using UnityEngine;

public class TubeScript : MonoBehaviour
{
    // Declare variables for the Mesh and MeshFilter
    Mesh mesh;
    MeshFilter meshFilter;

    void Start()
    {
        // Initialize the Mesh and MeshFilter
        mesh = new Mesh();
        meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        // Create the vertices of the tube
        int numVertices = 36;
        Vector3[] vertices = new Vector3[numVertices];
        float radius = 0.5f;
        float angle = 0f;
        float angleStep = (2f * Mathf.PI) / (numVertices / 2);
        for (int i = 0; i < numVertices / 2; i++)
        {
            // Define the vertices on the top of the tube
            vertices[i] = new Vector3(radius * Mathf.Cos(angle), radius * Mathf.Sin(angle), 0.5f);

            // Define the vertices on the bottom of the tube
            vertices[i + numVertices / 2] = new Vector3(radius * Mathf.Cos(angle), radius * Mathf.Sin(angle), -0.5f);

            // Increase the angle for the next iteration
            angle += angleStep;
        }

        // Create the triangles of the tube
        int[] triangles = new int[numVertices * 3];
        for (int i = 0; i < numVertices / 2; i++)
        {
            // Define the triangles on the top of the tube
            triangles[i * 6] = i;
            triangles[i * 6 + 1] = (i + 1) % (numVertices / 2);
            triangles[i * 6 + 2] = i + numVertices / 2;

            // Define the triangles on the bottom of the tube
            triangles[i * 6 + 3] = (i + 1) % (numVertices / 2);
            triangles[i * 6 + 4] = (i + 1) % (numVertices / 2) + numVertices / 2;
            triangles[i * 6 + 5] = i + numVertices / 2;
        }

        // Assign the vertices and triangles to the Mesh
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }
}
